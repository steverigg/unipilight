
<?php


function openSocket()
{
  /* Get the port for the service. */
  $service_port = 50006;

  /* Get the IP address for the target host. */
  $address = 'localhost';

  /* Create a TCP/IP socket. */
  $socket = socket_create(AF_INET, SOCK_STREAM, SOL_TCP);
  if ($socket === false) 
  {
    echo "socket_create() failed: reason: " . socket_strerror(socket_last_error()) . "\n";
    return null;
  } 
  else 
  {
//    echo "socket created!";
    // do nothing for now
  }

  $result = socket_connect($socket, $address, $service_port);
  if ($result === false) 
  {
    echo "socket_connect() failed.\nReason: ($result) " . socket_strerror(socket_last_error($socket)) . "\n";
    return null;
  } 
  else 
  {
    //echo "socket connected!";
    // do nothing for now
    return $socket;
  }
}

function closeSocket($aSocket)
{
  socket_close($aSocket);
}


function doRainbow()
{
  $socket = openSocket();
  if (!$socket) return;

  socket_write($socket, "RAINBOW", 7);
  while ($out = socket_read($socket, 2048)) {
    //echo $out;
  }
  closeSocket($socket);
}

function doWheel()
{
  $socket = openSocket();
  if (!$socket) return;

  socket_write($socket, "WHEEL", 5);
  while ($out = socket_read($socket, 2048)) {
    //echo $out;
  }
  closeSocket($socket);
}

function doRed()
{
  $socket = openSocket();
  if (!$socket) return;

  socket_write($socket, "RED", 3);
  while ($out = socket_read($socket, 2048)) {
    //echo $out;
  }
  closeSocket($socket);
}

function doGreen()
{
  $socket = openSocket();
  if (!$socket) return;

  socket_write($socket, "GREEN", 5);
  while ($out = socket_read($socket, 2048)) {
    //echo $out;
  }
  closeSocket($socket);
}

function doBlue()
{
  $socket = openSocket();
  if (!$socket) return;

  socket_write($socket, "BLUE", 4);
  while ($out = socket_read($socket, 2048)) {
    //echo $out;
  }
  closeSocket($socket);
}

function doYellow()
{
  $socket = openSocket();
  if (!$socket) return;

  socket_write($socket, "YELLOW", 6);
  while ($out = socket_read($socket, 2048)) {
    //echo $out;
  }
  closeSocket($socket);
}

function doOrange()
{
  $socket = openSocket();
  if (!$socket) return;

  socket_write($socket, "ORANGE", 6);
  while ($out = socket_read($socket, 2048)) {
    //echo $out;
  }
  closeSocket($socket);
}

function doPurple()
{
  $socket = openSocket();
  if (!$socket) return;

  socket_write($socket, "PURPLE", 6);
  while ($out = socket_read($socket, 2048)) {
    //echo $out;
  }
  closeSocket($socket);
}

function doWhite()
{
  $socket = openSocket();
  if (!$socket) return;

  socket_write($socket, "WHITE", 5);
  while ($out = socket_read($socket, 2048)) {
    //echo $out;
  }
  closeSocket($socket);
}

function doCustom($red, $grn, $blu)
{
  $socket = openSocket();
  if (!$socket) return;

  socket_write($socket, "CUSTOM " . $red . " " . $grn . " " . $blu, 20);
  while ($out = socket_read($socket, 2048)) {
    //echo $out;
  }
  closeSocket($socket);
}

function doBrightness($bright)
{
  $socket = openSocket();
  if (!$socket) return;

  socket_write($socket, "BRIGHT " . $bright, 12);
  while ($out = socket_read($socket, 2048)) {
    //echo $out;
  }
  closeSocket($socket);
}

function doPulse()
{
  $socket = openSocket();
  if (!$socket) return;

  socket_write($socket, "PULSE", 5);
  while ($out = socket_read($socket, 2048)) {
    //echo $out;
  }
  closeSocket($socket);
}

function doStop()
{
  $socket = openSocket();
  if (!$socket) return;

  socket_write($socket, "STOP", 4);
  while ($out = socket_read($socket, 2048)) {
    
    //echo $out;
  }
  closeSocket($socket);
}



?>
