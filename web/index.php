<?php             
	include_once 'sockets.php';
	  
	$rainbow   	= $_POST['rainbow'];
	$wheel 	    = $_POST['wheel'];
	$stop 	  	= $_POST['stop'];
	$red 		  	= $_POST['red'];
	$green 	  	= $_POST['green'];
	$blue 	  	= $_POST['blue'];
	$yellow   	= $_POST['yellow'];
	$orange   	= $_POST['orange'];
	$purple   	= $_POST['purple'];
	$white 	  	= $_POST['white'];
	$custom   	= $_POST['custom'];
	$rVal		  	= $_POST['rVal'];
	$gVal   		= $_POST['gVal'];
	$bVal 		  = $_POST['bVal'];
	$pulse		  = $_POST['pulse'];
	$bright		  = $_POST['bright'];
	$brightVal	= $_POST['brightVal'];

	
	if($stop) {
	  doStop();
	}
	
	if($rainbow) {
	  doRainbow();
	}
	if($wheel) {
	  doWheel();
	}
	if($red) {
	  doRed();
	}
	if($green) {
	  doGreen();
	}
	if($blue) {
	  doBlue();
	}
	
	if($yellow) {
	  doYellow();
	}
	
	if($orange) {
	  doOrange();
	}
	if($purple) {
	  doPurple();
	}
	
	if($white) {
	  doWhite();
	}
	            
	if($custom) {		
		if(!$rVal) $rVal = 0; 
		if(!$gVal) $gVal = 0; 
		if(!$bVal) $bVal = 0;

		if($rVal < 0) $rVal = 0; elseif($rVal > 255) $rVal = 255;   
		if($gVal < 0) $gVal = 0; elseif($gVal > 255) $gVal = 255;   
		if($bVal < 0) $bVal = 0; elseif($bVal > 255) $bVal = 255;   
		
		doCustom($rVal, $gVal, $bVal);
	} 

	if($bright) {
		if(!$brightVal) $brightVal = 0.2; 
		if($brightVal < 0) $brightVal = 0; elseif($brightVal > 1) $brightVal = 1;
		doBrightness($brightVal);   	
	}

	if($pulse) {
		doPulse();
	}	           
	           
?>

<html>
	
	<head>
	  <title>UniPiLight</title>
	  <link rel="stylesheet" href="styles.css" type="text/css">
	
	  <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.0/jquery.min.js"></script>
	
	</head>
	
	
	<body>
	
	
	  <div class="main_panel">
	
	      <form name="frmFunctions" method="post" action="">
				<div>
		        <input name="wheel" type="submit" id="wheel" value="WHL" class="btn-lg">
				</div>
		
				<div>
		        <input name="red" type="submit" id="red" value="RED" class="btn-lg">
				</div>
		
				<div>
		        <input name="green" type="submit" id="green" value="GRN" class="btn-lg">
				</div>
		
				<div>
		        <input name="blue" type="submit" id="blue" value="BLU" class="btn-lg">
				</div>
		
				<div>
		        <input name="rainbow" type="submit" id="rainbow" value="RBW" class="btn-lg">
				</div>
		
				<div>
		        <input name="yellow" type="submit" id="yellow" value="YLW" class="btn-lg">
				</div>
		
				<div>
		        <input name="orange" type="submit" id="orange" value="ORG" class="btn-lg">
				</div>
		
				<div>
		        <input name="purple" type="submit" id="purple" value="PRP" class="btn-lg">
				</div>
				
				<div id="custom_colour_panel">
					<div>
						<input name="rVal" type="text" id="rVal" class="custom_colour_text" value="<?php echo $rVal; ?>" placeholder="red">
					</div>
					<div>
						<input name="gVal" type="text" id="gVal" class="custom_colour_text" value="<?php echo $gVal; ?>" placeholder="grn">
					</div>
					<div>
						<input name="bVal" type="text" id="bVal" class="custom_colour_text" value="<?php echo $bVal; ?>" placeholder="blu">
					</div>
					<div>
			        <input name="custom" type="submit" id="custom" value="CUST" class="btn-mid">
					</div>
				</div>
			
				<div class="btn-mid">
				</div>
				
				<div id="options_panel">				
					<div class="outer">
						<div class="inner">
							<input name="brightVal" type="text" id="brightVal" class="custom_inner_text" value="<?php echo $brightVal; ?>" placeholder="brt">
						</div>
						<div class="inner">
				      	<input name="bright" type="submit" id="bright" value="BRT" class="btn-mid-inner">
						</div>
					</div>				

					<div>
			        <input name="pulse" type="submit" id="pulse" value="PLS" class="btn-lg">
					</div>				

  				<div>
  		        <input name="white" type="submit" id="white" value="WHT" class="btn-lg">
  				</div>
		
					<div>
			        <input name="stop" type="submit" id="stop" value="STOP" class="btn-lg">
					</div>
				</div>
		  
	      </form>
	
	  </div>  
	</body>
	<script type="text/javascript">
		var cw;
		cw = $('.btn-lg').width();
		$('.btn-lg').css({
    		'height': cw + 'px'
		});
		
		cw = $('.custom_colour_text').width() / 2;
		$('.custom_colour_text').css({
    		'height': cw + 'px'
		});
		
		cw = $('.custom_inner_text').width() / 2;
		$('.custom_inner_text').css({
    		'height': cw + 'px'
		});

		cw = $('.btn-mid').width() / 2;
		$('.btn-mid').css({
    		'height': cw + 'px'
		});

		cw = $('.btn-mid-inner').width() / 2;
		$('.btn-mid-inner').css({
    		'height': cw + 'px'
		});

	</script>
</html>
