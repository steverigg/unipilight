#!/usr/bin/python
#
################################################################################
#
#
#
#
################################################################################
# Imports

import time
import unicornhat as u
import lightadmin as admin
import WebInterface

lightAdmin = admin.LightAdmin()

def main():
    inc = 0
    rising = True

    divisor = 100
    max_bright = 0.2
    wait_time = 0.05
    
    webIF = WebInterface.ServerManager(lightAdmin)
    webIF.start()
                
    running = True

    while running:
        
        if rising:
            inc = inc + 1
            if inc == divisor:
                rising = False
        else:
            inc = inc - 1
            if inc == 10:
                rising = True

        for y in range(8):
            for x in range(8):
                lightAdmin.updateColours(x, y)
                u.set_pixel(x, y, lightAdmin.getRed(), lightAdmin.getGreen(), lightAdmin.getBlue())

        if lightAdmin.getPulsing():
            u.brightness(max_bright / divisor * inc)
            time.sleep(wait_time)
        else:
            time.sleep(0.5)
            u.brightness(lightAdmin.getBrightness())

        u.show()
        

################################################################################
if __name__ == '__main__':
  main()
