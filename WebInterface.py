#!/usr/bin/python
#
################################################################################
#
#
#
#
################################################################################
# Imports
import socket
import SocketServer
import threading
import time

# constants
HOST = ''        # Symbolic name meaning all available interfaces
PORT = 50006              # Arbitrary non-privileged port


################################################################################
# classes for the web interface

################################################################################
# class to handle the received network messages
class ThreadedTCPRequestHandler(SocketServer.BaseRequestHandler):
  lightadmin = None
  def handle(self):
    data = self.request.recv(1024)        
#    print "Data: " + data
    if (data == "STOP"):
      self.lightadmin.setStopped()
      self.request.sendall("STOPPED")
    elif (data == "RAINBOW"):
      self.lightadmin.setRainbowState()
      self.request.sendall("RAINBOW")
    elif (data == "WHEEL"):
      self.lightadmin.setColourWheelState()
      self.request.sendall("WHEEL")
    elif (data == "RED"):
      self.lightadmin.setRedState()
      self.request.sendall("RED")
    elif (data == "GREEN"):
      self.lightadmin.setGreenState()
      self.request.sendall("GREEN")
    elif (data == "BLUE"):
      self.lightadmin.setBlueState()
      self.request.sendall("BLUE")
    elif (data == "YELLOW"):
      self.lightadmin.setYellowState()
      self.request.sendall("YELLOW")
    elif (data == "ORANGE"):
      self.lightadmin.setOrangeState()
      self.request.sendall("ORANGE")
    elif (data == "PURPLE"):
      self.lightadmin.setPurpleState()
      self.request.sendall("PURPLE")
    elif (data == "WHITE"):
      self.lightadmin.setWhiteState()
      self.request.sendall("WHITE")
    elif (data == "PULSE"):
      result = self.lightadmin.invertPulsing()
      self.request.sendall("PULSE")
    elif (data[:6] == "BRIGHT"):
      chunks = data.split()
      self.lightadmin.setBrightness(float(chunks[1]))
      self.request.sendall("BRIGHTNESS")
    elif (data[:6] == "CUSTOM"):
      chunks = data.split()
      self.lightadmin.setCustomState(int(chunks[1]), int(chunks[2]), int(chunks[3]))
      self.request.sendall("CUSTOM RGB")
    elif (data == "TEST"):
      time.sleep(0.01)
      self.request.sendall("OK")        
      

################################################################################
# factory class for handling the thread
class ThreadedTCPServer(SocketServer.ThreadingMixIn, SocketServer.TCPServer):
  pass

################################################################################
# server manager class
#  handles threading of the web interface
#  takes the pirad admin class to send messages to
class ServerManager():
  def __init__(self, anAdminObject):
    class ThreadedTCPRequestHandlerWithAdmin(ThreadedTCPRequestHandler):
      lightadmin = anAdminObject
    self.server = ThreadedTCPServer((HOST, PORT), ThreadedTCPRequestHandlerWithAdmin)
    
  def start(self):             
    server_thread = threading.Thread(target=self.server.serve_forever)
    server_thread.start()

  def stop(self):
    self.server.shutdown()    
