#!/usr/bin/python
#
################################################################################
#
#
#
#
################################################################################
# Imports

import math

class LightAdmin:

    ##############################################################################
    # initialise state
    def __init__(self):
        self.brightness = 0.1
        self.pulsing = False
        self.red = 0
        self.green = 0
        self.blue = 0
        self.style = 2 # 1=solid; 2=color wheel; 3=rainbow
        self.i = 0.0
    
    def setRed(self, value):
        self.red = value
        
    def setGreen(self, value):
        self.green = value

    def setBlue(self, value):
        self.blue = value

    def setBrightness(self, value):
        self.brightness = value

    def setPulsing(self, value):
        self.pulsing = value
        
    def setStopped(self):
        self.style = 1
        self.pulsing = False
        self.setRed(0)
        self.setGreen(0)
        self.setBlue(0)

    def setRedState(self):
        self.style = 1
        self.setRed(255)
        self.setGreen(0)
        self.setBlue(0)

    def setGreenState(self):
        self.style = 1
        self.setRed(0)
        self.setGreen(255)
        self.setBlue(0)

    def setBlueState(self):
        self.style = 1
        self.setRed(0)
        self.setGreen(0)
        self.setBlue(255)

    def setYellowState(self):
        self.style = 1
        self.setRed(255)
        self.setGreen(255)
        self.setBlue(0)

    def setOrangeState(self):
        self.style = 1
        self.setRed(255)
        self.setGreen(165)
        self.setBlue(0)

    def setPurpleState(self):
        self.style = 1
        self.setRed(255)
        self.setGreen(0)
        self.setBlue(255)

    def setWhiteState(self):
        self.style = 1
        self.setRed(255)
        self.setGreen(255)
        self.setBlue(255)

    def setCustomState(self, rVal, gVal, bVal):
        self.style = 1
        self.setRed(rVal)
        self.setGreen(gVal)
        self.setBlue(bVal)

    def setRainbowState(self):
        self.pulsing = False
        self.style = 3
        self.i = 0.0

    def setColourWheelState(self):
        self.pulsing = False
        self.style = 2
        self.i = 0.0

    def updateRainbow(self, x, y):
        offset = 30
        if x == 0 and y == 0:
            self.i = self.i + 0.3
        r = (math.cos((x+self.i)/2.0) + math.cos((y+self.i)/2.0)) * 64.0 + 128.0
        g = (math.sin((x+self.i)/1.5) + math.sin((y+self.i)/2.0)) * 64.0 + 128.0
        b = (math.sin((x+self.i)/2.0) + math.cos((y+self.i)/1.5)) * 64.0 + 128.0
        self.red = int(max(0, min(255, r + offset)))
        self.green = int(max(0, min(255, g + offset)))
        self.blue = int(max(0, min(255, b + offset)))

    def updateColourWheel(self, x, y):
        scaledHue = int((self.i / 64) * 6)
        sector = scaledHue >> 8
        offsetInSector = scaledHue - (sector << 8)
        p = (255 * ( 255 - 255 )) >> 8
        q = (255 * ( 255 - ((255 * offsetInSector) >> 8) )) >> 8
        t = (255 * ( 255 - ((255 * ( 255 - offsetInSector )) >> 8) )) >> 8
        if sector == 0:
            oR = 255
            oG = t
            oB = p
        elif sector == 1:
            oR = q
            oG = 255
            oB = p
        elif sector == 2:
            oR = p
            oG = 255
            oB = t
        elif sector == 3:
            oR = p
            oG = q
            oB = 255
        elif sector == 4:
            oR = t
            oG = p
            oB = 255
        else:
            oR = 255
            oG = p
            oB = q

        self.red = int(oR)
        self.green = int(oG)
        self.blue = int(oB)

        if self.i > 255 * 64:
            self.i = 0.0;
        else:
            self.i = self.i + 1.0

    def updateColours(self, x, y):
        if self.style == 2:
            self.updateColourWheel(x, y)
        if self.style == 3:
            self.updateRainbow(x, y)
    
    def getRed(self):
        return self.red

    def getGreen(self):
        return self.green

    def getBlue(self):
        return self.blue

    def getBrightness(self):
        return self.brightness        

    def getPulsing(self):
        if self.style <> 1:
            return False
        else:
            return self.pulsing

    def invertPulsing(self):
        if self.style <> 1:
            return False
        else:
            self.setPulsing(not self.pulsing)
            return self.pulsing
                        
